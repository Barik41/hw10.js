// Реалізувати перемикання вкладок (таби) на чистому Javascript.
// У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. 
// При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// Потрібно передбачити, що текст на вкладках може змінюватись,  і що вкладки можуть додаватися та видалятися.
//  При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.
let tab = function () {
    let tabNav = document.querySelectorAll(".tabs-title"),
      tabContent = document.querySelectorAll(".tab"),
      tabName; 
  
    tabNav.forEach((item) => {
      item.addEventListener("click", selectTabNav);
    });
  
    function selectTabNav() {
      tabNav.forEach((item) => {
        item.classList.remove("active");
      });
      this.classList.add("active");
      tabName = this.getAttribute("data-tab-name");
      selectTabContent(tabName);
    }
  
    function selectTabContent(tabName) {
      tabContent.forEach((item) => {
        if (item.getAttribute("data-tab-name") === tabName) {
          item.classList.add("active");
        } else {
          item.classList.remove("active");
        }
      });
    }
};
  
tab();
  

  